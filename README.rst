Xabber-OTR-Certificate-Edition - an XMPP client for Android
================================

This is under improvement! This is a twisted version of the open source Jabber android client available here: https://github.com/redsolution/xabber-android
We try to add support for clients that have certificates. This would match only particular usages. Otherwise stick to using the initial implementation available at the above link. More descrition on our work can be found soon on the report available this page: http://kth.diva-portal.org/smash/search.jsf?dswid=9073

Using this implementation requries more work to make it functional. 

• First and foremost need is to establish a Certificate Authority (CA) and then sign user certificates signing requests (CSR). 
• Next there is a need to automate the process of having user's app send the CSR to the CA.

Refer to above-mentioned report to know the areas of future work to make this fully functional.

Open source Jabber (XMPP) client with multi-account support, clean and simple interface.lireza will improve this. 
Being both free (as in freedom!) and ad-free, Xabber is designed to be the best Jabber client for Android.

Supported protocols
===================

* RFC-3920: Core
* RFC-3921: Instant Messaging and Presence
* XEP-0030: Service Discovery
* XEP-0128: Service Discovery Extensions
* XEP-0115: Entity Capabilities
* XEP-0054: vcard-temp
* XEP-0153: vCard-Based Avatars
* XEP-0045: Multi-User Chat (incompletely)
* XEP-0078: Non-SASL Authentication
* XEP-0138: Stream Compression
* XEP-0203: Delayed Delivery
* XEP-0091: Legacy Delayed Delivery
* XEP-0199: XMPP Ping
* XEP-0147: XMPP URI Scheme Query Components
* XEP-0085: Chat State Notifications
* XEP-0184: Message Delivery Receipts
* XEP-0155: Stanza Session Negotiation
* XEP-0059: Result Set Management
* XEP-0136: Message Archiving
* XEP-0224: Attention

Translations
============

We use webtranslateit.com as our translation system.
All related resources are automatically generated from files got with webtranslateit.com.
If you want to update any translation please email us to info[at]xabber.com and specify all the translation languages you want to edit.
Then we'll send an invitation to our project on webtranslateit.com.
Please don't create pull requests with translation fixes as any changes will be overwritten with the next update from webtranslateit.com.