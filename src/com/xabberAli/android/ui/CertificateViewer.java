package com.xabberAli.android.ui;

import java.security.KeyPair;
import java.util.Collection;
import java.util.logging.Logger;

import net.java.otr4j.OtrException;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.xabberAli.android.data.account.AccountManager;
import com.xabberAli.android.data.account.OnAccountChangedListener;
import com.xabberAli.android.data.entity.BaseEntity;
import com.xabberAli.android.data.intent.EntityIntentBuilder;
import com.xabberAli.android.data.roster.OnContactChangedListener;
import com.xabberAli.android.ui.helper.ManagedActivity;
import com.xabberAli.android.data.account.AccountManager;

//public class CertificateViewer {
public class CertificateViewer extends ManagedActivity implements OnAccountChangedListener, OnContactChangedListener, OnClickListener {

	// private static final String EXTRA_FIELD_SHOW_EMAIL =
	// "com.xabberAli.android.data.ui.QuestionViewer.SHOW_QUESTION";
	private static final String EMAIL_ADDRESS = "EMAIL_ADDRESS";
	private static final String VOUCHER_STRING = "VOUCHER_STRING";
	// TODO complete with other things being saved as well.

	private String emailAddress; // Users bill before tip
	private String voucherString; // Tip amount

	// String url = "https://172.31.212.101/cgi-bin/ltca";
	// URL ltcaURL = new URL(url);

	EditText emailET;
	EditText voucherET;
	TextView voucherReqStatTV;
	TextView certReqStatTV;
	TextView largeResulTV;

	Button requestVoucherButton;
	Button requestCertificateButton;

	// logger.info("Alireza Debug: ");

	// These few lines are added. Taken from OTRManager.java.
	/*
	 * KeyPair getMyKeyPair() throws OtrException { KeyPair keyPair =
	 * AccountManager.getInstance().getAccount(account).getKeyPair();
	 * 
	 * if (keyPair == null) throw new OtrException(new
	 * IllegalStateException("KeyPair is not ready, yet."));
	 * logger.info("Alireza Talking info."); return keyPair; }
	 */
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		/*Logger logger = Logger.getLogger(CertificateViewer.class.getName());
		KeyPair keyPair = AccountManager.getInstance().getAccount(account).getKeyPair();
		logger.info("Alireza debug: " + );*/
		
	}

	@Override
	public void onContactsChanged(Collection<BaseEntity> entities) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAccountsChanged(Collection<String> accounts) {
		// TODO Auto-generated method stub

	}

	/**
	 * @param context
	 * @param account
	 * @param user
	 * @param showQuestion
	 *            <code>false</code> is used for shared secret.
	 * @param answerRequest
	 *            <code>false</code> is used to ask a question.
	 * @param question
	 *            must be not <code>null</code> if showQuestion and
	 *            answerRequest are <code>true</code>.
	 * @return
	 */
	public static Intent createIntent(Context context, String account, String user) {
		Intent intent = new EntityIntentBuilder(context, QuestionViewer.class).setAccount(account).setUser(user).build();
		// intent.putExtra(EXTRA_FIELD_SHOW_EMAIL, showEmail);
		return intent;
	}

}